package com.gitlab.fwilhe.docconv

class Converter(val outputFormat: OutputFormat) {

    fun convert(input: String): String {
        return outputFormat.format(Parser().parse(input))
    }
}

private class Parser {
    fun parse(input: String): InternalDocumentRepresentation {
        return InternalDocumentRepresentation(
                InternalDocumentRepresentation.Title(input.lines().first()),
                InternalDocumentRepresentation.Body(input.removePrefix(input.lines().first()))
        )
    }
}

class InternalDocumentRepresentation(val title: Title, val body: Body) {

    class Title(val text: String)

    class Body(val text: String)
}

interface OutputFormat {
    fun format(documentRepresentation: InternalDocumentRepresentation): String
}

class PlainText : OutputFormat {
    override fun format(documentRepresentation: InternalDocumentRepresentation): String {
        return documentRepresentation.title.text + documentRepresentation.body.text
    }
}

class Html : OutputFormat {
    override fun format(documentRepresentation: InternalDocumentRepresentation): String {
        return """<title>${documentRepresentation.title.text}</title>
<body>
${documentRepresentation.body.text.removePrefix("\n")}
</body>"""
    }

}
