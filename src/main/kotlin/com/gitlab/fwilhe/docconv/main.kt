package com.gitlab.fwilhe.docconv

import java.io.InputStreamReader

fun main(args: Array<String>) {
    InputStreamReader(System.`in`).use {
        if (args.isEmpty()) {
            println(Converter(PlainText()).convert(it.readText()))
        } else if (args[0] == "html") {
            println(Converter(Html()).convert(it.readText()))
        }
    }
}
