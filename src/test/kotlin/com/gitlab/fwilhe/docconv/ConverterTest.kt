package com.gitlab.fwilhe.docconv

import org.junit.Assert.*
import org.junit.Test

class ConverterTest {
    @Test fun `Input equals output with PlainText format`() {
        val text = "This is _some_ text"
        assertEquals(text, Converter(PlainText()).convert(text))
    }

    @Test fun `Multi line input equals output with PlainText format`() {
        val text = """= some Text
with several
lines"""
        assertEquals(text, Converter(PlainText()).convert(text))
    }

    @Test fun `Multi line input equals rendered output with Html format`() {
        val input = """= some Text
with several
lines"""
        val expected = """<title>= some Text</title>
<body>
with several
lines
</body>"""
        assertEquals(expected, Converter(Html()).convert(input))
    }
}